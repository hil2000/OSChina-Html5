define(['text!project/software-list.html', "../base/openapi", '../base/util'],
	function(viewTemplate, OpenAPI, Util) {
		return Piece.View.extend({
			id: 'project-software-list',
			events: {
				"click .backBtn": "goBack",
				"click .projectList": "goToDetailList",
			},
			goBack: function() {
				var lastPage = Piece.Session.loadObject("osLastPage");
				this.navigateModule(lastPage, {
					trigger: true
				});
			},
			goToDetailList: function(el) {
				var $target = $(el.currentTarget);
				var tag = $target.attr("data-tag");
				var name = $target.attr("data-name");
				name = encodeURI(name);
				this.navigate("software-detail-list?tag=" + tag + "&name=" + name, {
					trigger: true
				});
			},
			render: function() {
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				Util.loadList(this, 'project-software-list', OpenAPI.project_catalog_list, {
					'tag': 0,
					'dataType': OpenAPI.dataType,
					'page': 1,
					'pageSize': 100
				});
				//write your business logic here :)
			}
		}); //view define

	});