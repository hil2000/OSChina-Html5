// define(['text!../news/blog-comment-list.html', "../base/openapi", '../base/util', '../base/components/footer-menu-detail'],
// 	function(viewTemplate, OpenAPI, Util, MenuDetail) {
// 		return Piece.View.extend({
// 			menu: null,
// 			events: {
// 				"click .tweetList": "goToReply",
// 				"click .homeBtn": "goBackToHome",
// 				"click .refreshBtn": "refreshCommentList"

// 			},
// 			refreshCommentList: function() {
// 				this.loadCommentList();
// 			},
// 			goBackToHome: function() {
// 				window.history.back();
// 			},
// 			goToReply: function(el) {
// 				var $target = $(el.currentTarget);
// 				var tweetId = Util.request("id");
// 				var commentAuthorId = $target.attr("data-commentAuthorId");
// 				var commentId = $target.attr("data-commentId");

// 				var dataSource = "cube-list-blog_comment_list";
// 				var catalog = "blog";
// 				this.navigateModule("common/comment-reply?tweetId=" + tweetId + "&commentAuthorId=" + commentAuthorId + "&commentId=" + commentId + "&dataSource=" + dataSource + "&catalog=" + catalog, {
// 					trigger: true
// 				});
// 			},
// 			render: function() {
// 				$(this.el).html(viewTemplate);

// 				Piece.View.prototype.render.call(this);
// 				return this;
// 			},
// 			onShow: function() {
// 				var me = this;
// 				var footerTemplate = $(me.el).find("#footerTemplate").html();

// 				var footerHtml = _.template(footerTemplate, {
// 					"id": Util.request("id"),
// 					"url": "common/common-comment",
// 					"activeIndex": 2
// 				});
// 				$("body").append(footerHtml);
// 				menu = new MenuDetail();
// 				// favorite state
// 				// var favorite = Util.request("favorite");
// 				// console.info(favorite + "===========");
// 				// if (favorite ==="false") {
// 				// 	$(".tab-item").find('.collect').removeClass("icon-star-empty").addClass("icon-star");
// 				// 	$('.icon-star').css("color", "#0882f0");
// 				// } else {
// 				// 	$(".tab-item").find('.collect').removeClass("icon-star").addClass("icon-star-empty");
// 				// 	$('.collect').css("color", "white");
// 				// }
// 				var token = Piece.Store.loadObject("user_token");
// 				if (token !== null) {
// 					var accesstoken = token.access_token;
// 					var checkDetail = Util.request("checkDetail");
// 					switch (checkDetail) {
// 						case "news/news-detail":
// 							{
// 								var backdatail = "news_detail";
// 								break;
// 							}
// 						case "news/news-blog-detail":
// 							{
// 								var backdatail = "news_blog_detail";
// 								break;
// 							}
// 						case "question/question-detail":
// 							{
// 								var backdatail = "question_detail";
// 							}
// 							break;
// 					}
// 					Util.Ajax(OpenAPI[backdatail], "GET", {
// 						id: Util.request("id"),
// 						dataType: 'jsonp',
// 						access_token: accesstoken
// 					}, 'json', function(data, textStatus, jqXHR) {
// 						if (data) {
// 							var favorite = data.favorite;
// 							if (favorite === 1) {
// 								$(".tab-item").find('.collect').removeClass("icon-star-empty").addClass("icon-star");
// 								$('.icon-star').css("color", "#0882f0");
// 							}
// 							if (data.commentCount != 0) {
// 								$('.commentImg').css("display", "block");
// 								$('.commentCount').html(data.commentCount)
// 							}
// 						}
// 					}, null, function(xhr, status) {
// 						me.loadCommentList();
// 					});
// 				} else {
// 					this.loadCommentList();
// 				}
// 			},
// 			gobackBtn: function() {
// 				var id = Util.request("id");
// 				this.navigateModule("news/news-detail?id=" + id, {
// 					trigger: true
// 				});
// 			},
// 			loadCommentList: function() {
// 				Util.loadList(this, 'blog_comment_list', OpenAPI.blog_comment_list, {
// 					'id': Util.request("id"),
// 					'dataType': OpenAPI.dataType,
// 					'page': 1,
// 					'pageSize': OpenAPI.pageSize
// 				}, true);
// 			}
// 		}); //view define

// 	});