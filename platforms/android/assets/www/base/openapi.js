define(['zepto'], function($) {

	var ip = "http://www.oschina.net";
	var me = this;
	// $.ajax({
	// 	timeout: 2000 * 1000,
	// 	async: false,
	// 	url: "http://runjs.cn/action/ip/get?k=mysql",
	// 	dataType: "text",
	// 	beforeSend: function(xhr, settings) {

	// 	},
	// 	success: function(data, textStatus, jqXHR) {
	// 		ip = "http://" + data + ":81";
	// 	},
	// 	error: function(e, xhr, type) {
	// 		alert("获取服务器地址失败。");
	// 	},
	// 	complete: function(xhr, status) {}
	// });

	// "client_id": "oschina.app",
	// "client_secret": "bgMpYqNJYP0Qlv1Zi38MsbP3pPltDckX",

	// "client_id": "s5bEf6iBvJ0EQmTitbPb",
	// 	"client_secret": "YJujaPKt5faEg7VVWhzYFfWZoBkBCsD1",

	var OpenAPI = {
		"dataType": "jsonp",
		"pageSize": 10,
		"client_id": "s5bEf6iBvJ0EQmTitbPb",
		"client_secret": "YJujaPKt5faEg7VVWhzYFfWZoBkBCsD1",

		"access_token": ip + "/action/openapi/token",
		"access_user": ip + "/action/openapi/user",
		"user_information": ip + "/action/openapi/user_information",
		"my_information": ip + "/action/openapi/my_information",
		/*"my_information": ipLocal + "/action/openapi/my_information",*/
		/*暂用本地接口 等oschina接口出现换*/



		"news_list": ip + "/action/openapi/news_list",
		"news_blog": ip + "/action/openapi/blog_list",
		"news_recommend": ip + "/action/openapi/blog_recommend_list",
		"news_detail": ip + "/action/openapi/news_detail",
		"news_blog_detail": ip + "/action/openapi/blog_detail",
		"news_search": ip + "/action/openapi/search_list",


		"post_pub": ip + "/action/openapi/post_pub",
		"question_list": ip + "/action/openapi/post_list",
		"question_detail": ip + "/action/openapi/post_detail",

		"tweet_list": ip + "/action/openapi/tweet_list",
		"tweet_detail": ip + "/action/openapi/tweet_detail",
		"tween_hot_list": ip + "/action/openapi/tweet_list",
		"tweet_pub": ip + "/action/openapi/tweet_pub",
		"tweet_delete": ip + "/action/openapi/tweet_delete",

		"my_list": ip + "/action/openapi/active_list",
		'message_list': ip + '/action/openapi/message_list',
		"message_delete": ip + "/action/openapi/message_delete",

		"comment_pub": ip + "/action/openapi/comment_pub",
		"comment_reply": ip + "/action/openapi/comment_reply",
		"comment_list": ip + "/action/openapi/comment_list",
		"comment_delete": ip + "/action/openapi/comment_delete",

		"favorite_list": ip + "/action/openapi/favorite_list",
		"favorite_add": ip + "/action/openapi/favorite_add",
		"favorite_remove": ip + "/action/openapi/favorite_remove",

		"blog_comment_list": ip + "/action/openapi/blog_comment_list",
		"blog_comment_pub": ip + "/action/openapi/blog_comment_pub",
		"blog_comment_reply": ip + "/action/openapi/blog_comment_reply",


		"user_relation": ip + "/action/openapi/update_user_relation",
		"user_dynmic_blog": ip + "/action/openapi/user_blog_list",
		"user_blog_delete": ip + "/action/openapi/user_blog_delete",


		"project_catalog_list": ip + "/action/openapi/project_catalog_list",
		"project_list": ip + "/action/openapi/project_list",
		"project_detail": ip + "/action/openapi/project_detail",
		"project_tag_list": ip + "/action/openapi/project_tag_list",

		"friends_list": ip + "/action/openapi/friends_list",

		"clear_notice": ip + "/action/openapi/clear_notice",
		"portrait_update": ip + "/action/openapi/portrait_update",
		"user_notice": ip + "/action/openapi/user_notice"

	};

	return OpenAPI;

});