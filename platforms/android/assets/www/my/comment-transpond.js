define(['text!../my/comment-transpond.html', "../base/openapi", '../base/util', '../base/login/login'],
	function(viewTemplate, OpenAPI, Util, Login) {
		return Piece.View.extend({
			id: 'tweet_comment-message',
			login: null,
			events: {
				"click .backBtn": "goBack",
				"click .reply": "commentReply"
			},
			goBack: function() {
				history.back();
			},
			commentReply: function() {
				Util.checkLogin();
				var checkLogin = Util.checkLogin();
				if (checkLogin === false) {
					login.show();
				} else {
					this.commentReply1();
				}
			},
			commentReply1: function() {
				var me = this;
				var reciever = $("#reciever").val();
				reciever = reciever.replace(/(\s*$)/g, "");
				if (reciever == "") {
					new Piece.Toast("请输入对方的昵称");
					return;
				}
				var replyCont = $(".replyTextarea").val();
				replyCont = replyCont.replace(/(\s*$)/g, ""); //去掉右边空格
				var isNotice = $(".replyNotice").is(":checked");;
				if (replyCont === "" || replyCont === " ") {
					new Piece.Toast('请输入评论内容');
				} else {

					//通过用户名查询用户ID
					Util.Ajax(OpenAPI.user_information, "GET", {
						friend_name: reciever,
						dataType: 'jsonp'
					}, 'json', function(data) {
						// console.info(data);
						if (data != null) {
							me.comment_pub(data.user);
							// console.info(typeof data.user);
						} else {
							new Piece.Toast("查无此用户");
							return;
						}

					}, null, null);


				}

			},
			comment_pub: function(recieverId) {
				var commentCont = $(".replyTextarea").val();
				var userToken = Piece.Store.loadObject("user_token");
				var accessToken = userToken.access_token;
				// var tweetId = Util.request("userId");

				Util.Ajax(OpenAPI.comment_pub, "GET", {
					access_token: accessToken,
					id: recieverId,
					catalog: 4,
					content: commentCont,
					dataType: 'jsonp'
				}, 'json', function(data) {
					console.info(data);
					if (data.error == "200") {
						new Piece.Toast("留言发表成功");
						setTimeout(function() {
							history.back();

						}, 500);

					} else {
						if (data.error_description !== "") {
							new Piece.Toast(data.error_description);
						} else {
							new Piece.Toast("请求出错，请稍后再试！");
						}

					}



				}, null, null);
			},
			render: function() {
				login = new Login();
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				var me = this;
				var friendname = Util.request("friendname");
				var content = Util.request("content");
                // console.info(transContent);
				friendname = decodeURI(friendname);
				console.info(friendname);
				content = decodeURI(content);
				console.info(friendname,name);
				var transContent = "@"+friendname+" "+content;
                // alert(transContent);
				$(".replyTextarea").val("");
				// alert(transContent);
				$(".replyTextarea").val(transContent);
				$('#reciever').focus();
				// 小屏幕出现小键盘后textarea布局改变问题
				// $(window).resize(function() {
				// 	me.showkeyboard();

				// });

			}
			// showkeyboard:function(){
			// 	var availHeight = window.screen.availHeight ;
			// 	if(availHeight<230){
			// 		$('.replyTextarea').css("height","65px")
			// 	}

			// }
		}); //view define

	});